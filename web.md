LISPER
======
-------
###一、基础知识 - The basics
 
  这是一个基于restas的web-app，就像看到的那样，**她**非常**简单**、**直接**。（将下列代码保存为hello-world.lisp）
```
 ;;;; hello-world.lisp 
 (ql:quickload "restas") 
 
 (restas:define-module #:hello-world
     (:use :cl :restas))
 
 (in-package #:hello-world)
 
 (define-route hello-world ("")
   "Hello World")
 
 (start '#:hello-world :port 8080)
```
你可以按照下列两种方式来启动web-app：
```
 $ sbcl --load hello-world.lisp
```
或者先进入sbcl，然后load
```
$ sbcl 
<进入sbcl的提示信息>
* (load "hello-world.lisp")
```
两种方式都可以，根据自己的情况来。
启动成功后就可以通过http://localhost:8080/来访问了。  
>  访问效果：服务器返回"hello world"的字符串，没有其他的内容。虽然有点遗憾，但这是一个不错的开始。

###二、代码详细解释
```
(ql:quickload "restas")
```
加载restas，它是一个web开发框架。后续的程序都是基于restas，所以这行代码将会经常看到。

```
 (restas:define-module #:hello-world
     (:use :cl :restas))
 
 (in-package #:hello-world)
```
###小技巧
编码处理：
(load "restas-test" :external-format '(:gbk))

