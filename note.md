(in-package :cl-user)


(defpackage :cn.sz.jyliang.lisp.test
 	(:use :common-lisp)
	(:export
	 	:foo
		:testa))

;;创建新包mine，并且切换至这个新包。另一种表达方式
;;(setf *package* (make-package 'mine
;;	:use '(common-lisp)))

;;私有变量的访问
;;common-lisp-user::sym
;;公共变量的访问(A)，区别仅仅是少了一个冒号。那么，如何才能少一个冒号呢？
;;需要export，见下下行。
;;common-lisp-user:sym
;;export操作:
;;	(in-package cl-user)
;;	(export 'say)
;;	(setf say "hello")
;;export使用:
;;	(in-package mine)
;;	mine>cl-user:say
;;	"hello"
;;公共变量的访问(B)，区别很大，少写很多东西。如何做到的呢？需要import。
;;mine>say
;;"hello"
;;import操作:
;;	cl-user>(in-package mine)
;;	mine>(import 'cl-user:say)
;;	mine>say
;;	"hello"
;;公共变量的访问(C)，效果和B一样，但是操作更加方便。不需要每个符号都指定。
;;怎么做？需要use-package。更多的时候它是以:use参数存在。
;;	cl-user>(in-package mine)
;;	mine>(use-package 'cl-user)
;;	mine>say
;;	"hello"
;;	mine>...cl-user中其他符号也可以...
